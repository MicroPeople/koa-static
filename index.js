const Koa = require("koa");
const http = require("http");
const static = require("./koa-static-router");
// const router = require("koa-router")();
const app = new Koa();

// 单个路由
// app.use(static({
//     dir:'public',
//     router:'/static/'     //路由长度 =1
// }))

//多个路由
app.use(
  static([
    {
      dir: "public",
      router: "/public/image/",
    },
    {
      dir: "static",
      router: "/static/image/",
    },
  ])
);
// app.use(async (ctx) => {
//   ctx.body = "hello world";
// });

// app.use(router.routes()); //作用：启动路由

app.listen(3030, () => {
  console.log("build success");
});

// http.createServer(app.callback()).listen(3030);
console.log("the server is running on 3030");
