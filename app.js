const Koa = require("koa");
const path = require("path");
const serve = require("koa-static");
const Router = require("koa-router");

const app = new Koa();
const router = new Router();
// 静态资源
const jingdong = serve(path.join(__dirname) + "/public/jingdong");
// const vue = serve(path.join(__dirname) + "/public/vue");

// app.use(home);
app.use(async (ctx) => {
  ctx.body = "static file server";
});

app.listen(3030, () => {
  console.log("build success");
});

app.use(router.get("/jingdong/", jingdong));
// app.use(route.get("/vue/", vue));
